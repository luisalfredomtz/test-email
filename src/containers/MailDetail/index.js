import React, { Component } from 'react'
import './MailDetail.css'
import moment from 'moment'
import 'moment/locale/es'
import { connect } from 'react-redux'
import { setStatusMail } from './../../actionCreators'

export class MailDetail extends Component {
	constructor() {
		super()
		this.state = {}
	}

	render() {
		var numKeys = Object.keys(this.props.mail).length
		return (
			<div className="MailDetail">
				{numKeys !== 0 && (
					<div>
						<div className="headerDetail">
							<label className="subjectLabel">{this.props.mail.subject}</label>
							<div className="Actions">
								{this.props.tabs.map((item, i) => (
									<span key={i}>
										<div
											onClick={() =>
												this.props.setStatusMail(item.idAction, this.props.mail)
											}
											className="inner"
										>
											<i
												style={{
													marginLeft: 10,
													marginRight: 10,
													fontSize: 19,
													color: '#8B8B8B'
												}}
												id={item.class}
												className={
													item.name === 'Inbox' &&
													this.props.tabSelect === 'Inbox'
														? 'fa fa-circle'
														: item.class
												}
												aria-hidden="true"
											/>
										</div>
									</span>
								))}
							</div>
						</div>
						<div className="ContentEmail">
							<div className="InfoEmail">
								<div className="FromEmail">
									<div>
										<span style={{ marginRight: 5 }}>
											{this.props.mail.from}
										</span>
										<a href={'mailto:' + this.props.mail.from}>
											{'<' + this.props.mail.from + '>'}
										</a>
									</div>
									<span className="email-date">
										{moment(this.props.mail.date).format('ll, h:mm a')}
									</span>
								</div>
								<div>
									<p>{this.props.mail.body}</p>
								</div>
								<div className="wrapperImages">
									{this.props.mail.attachements.map((item, i) => (
										<div key={i} style={{ margin: 10 }}>
											<img
												className="email-image"
												src="http://dummyimage.com/250x250.jpg/5fa2dd/ffffff"
												alt="Foo eating a sandwich."
											/>
										</div>
									))}
								</div>
							</div>
						</div>
					</div>
				)}
			</div>
		)
	}
}

const mapStateToProps = state => {
	return {
		mail: state.mailActive,
		tabs: state.menuTabs,
		indexMailActive: state.indexMailActive,
		tabSelect: state.tabSelect
	}
}

const mapDispatchToProps = dispatch => {
	return {
		setStatusMail(email, i) {
			dispatch(setStatusMail(email, i))
		}
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(MailDetail)
