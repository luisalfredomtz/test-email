import React, { Component } from 'react'

import './MailList.css'
import { connect } from 'react-redux'
import Tabs from './../../components/Tabs'

import { setMailActive, selectTab } from './../../actionCreators'

export class MailList extends Component {
	constructor(props) {
		super(props)
		this.state = {}
	}

	render() {
		return (
			<div className="MailList">
				<div className="HeaderList">
					<div className="wrapperActions">
						{this.props.tabs.map((item, i) => (
							<div className="inner" key={i}>
								<label
									className={item.active ? 'labelFilterActive' : 'labelFilter'}
									onClick={() => this.props.selectTab(item.name)}
								>
									<i
										style={{ marginRight: 5 }}
										id="to-spam"
										className={item.class}
										aria-hidden="true"
									/>
									{item.name}
								</label>
							</div>
						))}
					</div>
				</div>
				{
					{
						Inbox: <Tabs data={this.props.emails} />,
						Spam: <Tabs data={this.props.spam} />,
						Trash: <Tabs data={this.props.trash} />
					}[this.props.tabSelect]
				}
			</div>
		)
	}
}

const mapStateToProps = state => {
	return {
		emails: state.emails,
		trash: state.trash,
		tabs: state.menuTabs,
		spam: state.spam,
		tabSelect: state.tabSelect,
		correosActivos: state.correosActivos
	}
}

const mapDispatchToProps = dispatch => {
	return {
		setMailActive(email, i) {
			dispatch(setMailActive(email, i))
		},
		selectTab(name) {
			dispatch(selectTab(name))
		}
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(MailList)
