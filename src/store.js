import { createStore } from 'redux'

const menu = [
	{ idAction: 'INBOX', name: 'Inbox', class: 'fa fa-inbox', active: true },
	{ idAction: 'SPAM', name: 'Spam', class: 'fa fa-microchip', active: false },
	{ idAction: 'TRASH', name: 'Trash', class: 'fa fa-trash-o', active: false }
]

const reducer = (state, action) => {
	if (action.type === 'PUSH_EMAILS') {
		console.log('PUSH_EMAILS')
		return {
			...state,
			emails: state.emails.concat([
				{
					from: 'mhallatt0@walmart.com',
					to: 'cziem0@surveymonkey.com',
					subject: 'Office Assistant IV',
					body:
						'condimentum curabitur in libero ut massa volutpat convallis morbi odio odio elementum eu interdum eu tincidunt in leo maecenas pulvinar lobortis',
					date: '3/31/2017',
					isReaded: false,
					avatar:
						'https://robohash.org/dignissimosetsuscipit.jpg?size=50x50&set=set1',
					tag: 'Indigo',
					attachements: [
						{
							file: 'http://dummyimage.com/250x250.jpg/5fa2dd/ffffff',
							name: 'ut_nulla_sed.jpeg'
						},
						{
							file: 'http://dummyimage.com/250x250.jpg/5fa2dd/ffffff',
							name: 'ut_nulla_sed.jpeg'
						}
					]
				},
				{
					from: 'nmulbery1@seattletimes.com',
					to: 'idimont1@usa.gov',
					subject: 'Technical Writer',
					body:
						'sit amet cursus id turpis integer aliquet massa id lobortis convallis tortor risus dapibus augue vel accumsan tellus nisi eu orci mauris lacinia sapien quis libero nullam sit amet',
					date: '11/17/2016',
					isReaded: false,
					avatar:
						'https://robohash.org/aliquamautdolore.jpg?size=50x50&set=set1',
					tag: 'Teal',
					attachements: [
						{
							file: 'http://dummyimage.com/250x250.jpg/dddddd/000000',
							name: 'sodales_scelerisque_mauris.jpeg'
						}
					]
				}
			])
		}
	}
	if (action.type === 'SET_MAIL_ACTIVE') {
		console.log('SET_MAIL_ACTIVE')

		if (state.tabSelect === 'Inbox') {
			return {
				...state,
				mailActive: action.mailActive,
				indexMailActive: action.indexMailActive,
				emails: state.emails.map((item, i) => {
					if (i === action.indexMailActive.index) {
						item['isReaded'] = true
						item['old'] = i
					}
					return item
				})
			}
		}
		if (state.tabSelect === 'Spam') {
			return {
				...state,
				mailActive: action.mailActive,
				indexMailActive: action.indexMailActive,
				spam: state.spam.map((item, i) => {
					if (i === action.indexMailActive.index) {
						item['isReaded'] = true
					}
					return item
				})
			}
		}
		if (state.tabSelect === 'Trash') {
			return {
				...state,
				mailActive: action.mailActive,
				indexMailActive: action.indexMailActive,
				trash: state.trash.map((item, i) => {
					if (i === action.indexMailActive.index) {
						item['isReaded'] = true
					}
					return item
				})
			}
		}
	}

	if (action.type === 'SELECT_TAB') {
		console.log('SELECT_TAB')

		return {
			...state,
			mailActive: {}, //Para mantener seleccionado o no
			tabSelect: action.tabSelect,
			menuTabs: menu.map((tab, i) => {
				if (tab.name === action.tabSelect) {
					tab['active'] = true
				} else {
					tab['active'] = false
				}
				return tab
			})
		}
	}
	if (action.type === 'MOVE_TO_TRASH') {
		console.log('MOVE_TO_TRASH')

		if (state.tabSelect === 'Inbox') {
			return {
				...state,
				trash: state.trash.concat(action.mail),
				mailActive: {},
				emails: state.emails.filter(
					(item, i) => i !== state.indexMailActive.index
				)
			}
		}
		if (state.tabSelect === 'Spam') {
			return {
				...state,
				trash: state.trash.concat(action.mail),
				mailActive: {},
				spam: state.spam.filter((item, i) => i !== state.indexMailActive.index)
			}
		}
	}

	if (action.type === 'MOVE_TO_SPAM') {
		console.log('MOVE_TO_SPAM')

		if (state.tabSelect === 'Inbox') {
			return {
				...state,
				spam: state.spam.concat(action.mail),
				mailActive: {},
				emails: state.emails.filter(
					(item, i) => i !== state.indexMailActive.index
				)
			}
		}
		if (state.tabSelect === 'Trash') {
			return {
				...state,
				spam: state.spam.concat(action.mail),
				mailActive: {},
				trash: state.trash.filter(
					(item, i) => i !== state.indexMailActive.index
				)
			}
		}
	}
	if (action.type === 'MOVE_TO_INBOX') {
		console.log('MOVE_TO_INBOX')
		if (state.tabSelect === 'Inbox') {
			return {
				...state,
				mailActive: {},
				emails: state.emails.map((item, i) => {
					if (i === state.indexMailActive.index) {
						item['isReaded'] = false
					}
					return item
				})
			}
		}

		if (state.tabSelect === 'Spam') {
			state.emails.splice(action.mail.old, 0, action.mail)
			return {
				...state,
				mailActive: {},
				spam: state.spam.filter((item, i) => i !== state.indexMailActive.index)
			}
		}

		if (state.tabSelect === 'Trash') {
			state.emails.splice(action.mail.old, 0, action.mail)
			return {
				...state,
				mailActive: {},
				trash: state.trash.filter(
					(item, i) => i !== state.indexMailActive.index
				)
			}
		}
	}

	return state
}

export default createStore(reducer, {
	emails: [],
	mailActive: {},
	indexMailActive: {},
	menuTabs: menu,
	tabSelect: 'Inbox',
	trash: [],
	spam: []
})
