const setMailActive = (email, i) => {
	console.log(email, i)

	return {
		type: 'SET_MAIL_ACTIVE',
		mailActive: email,
		indexMailActive: { index: i }
	}
}
const selectTab = name => {
	return {
		type: 'SELECT_TAB',
		tabSelect: name
	}
}

const setStatusMail = (idAction, mail) => {
	return {
		type: 'MOVE_TO_' + idAction,
		mail: mail
	}
}

export { setMailActive, selectTab, setStatusMail }
