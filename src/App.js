import React, { Component } from 'react'
import './App.scss'
//containers
import MailList from './containers/MailList'
import MailDetail from './containers/MailDetail'
//Store
import store from './store'

class App extends Component {
	componentDidMount() {
		setInterval(() => this.pushEmails(), 9000)
	}

	pushEmails() {
		store.dispatch({
			type: 'PUSH_EMAILS'
		})
	}
	render() {
		return (
			<div className="App">
				<MailList />
				<MailDetail />
			</div>
		)
	}
}

export default App
