import React, { Component } from 'react'
import './Tabs.css'
import moment from 'moment'
import { connect } from 'react-redux'
import { setMailActive, selectTab } from './../../actionCreators'
export class Tabs extends Component {
	constructor(props) {
		super(props)
		this.state = {}
	}

	render() {
		var numKeys = Object.keys(this.props.mail).length
		return (
			<div className="ListContent">
				{this.props.data
					.map((email, i) => (
						<div
							key={i}
							onClick={() => this.props.setMailActive(email, i)}
							className={
								'InfoWraper ' +
								(this.props.indexMailActive.index === i && numKeys > 0
									? 'ItemSelect'
									: '')
							}
						>
							<div style={{ margin: 10 }}>
								<i
									style={{
										color: !email.isReaded ? '#4990E2' : 'transparent',
										fontSize: 12
									}}
									id="to-spam"
									className={'fa fa-circle'}
									aria-hidden="true"
								/>
							</div>
							<div className="EmailContainer">
								<div className="WraperDate">
									<div style={{ width: 245 }}>
										<label className="Sender">
											<i
												style={{
													fontSize: 12,
													marginRight: 3
												}}
												id="to-spam"
												className={'fa fa-user-o'}
												aria-hidden="true"
											/>
											{email.from}
										</label>
									</div>
									<div style={{ width: 50 }}>
										<label className="DateEmail">
											{moment().format('MMM D')}
										</label>
									</div>
								</div>
								<label className="Subject">{email.subject}</label>
								<label className="short-description ">{email.body}</label>
							</div>
						</div>
					))
					.reverse()}
			</div>
		)
	}
}

const mapStateToProps = state => {
	return {
		emails: state.emails,
		trash: state.trash,
		tabs: state.menuTabs,
		spam: state.spam,
		tabSelect: state.tabSelect,
		correosActivos: state.correosActivos,
		indexMailActive: state.indexMailActive,
		mail: state.mailActive
	}
}

const mapDispatchToProps = dispatch => {
	return {
		setMailActive(email, i) {
			dispatch(setMailActive(email, i))
		},
		selectTab(name) {
			dispatch(selectTab(name))
		}
	}
}
export default connect(mapStateToProps, mapDispatchToProps)(Tabs)
